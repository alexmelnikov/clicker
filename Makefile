CURL_FORM_PARAMS=-X POST -F key=LsdDFjn234rF78hfuI234SDfe789fhw -F pkg_type=file -F repo_name=ncc-clicker -F arch=

.PHONY: build deploy fix_permission

FULL_VERSION := $(shell git describe --long | cut -d- -f1,2)
BUILD_AT := $(shell date +%FT%T%z)
BUILD_COMMAND=go build -ldflags "-X main.commit=${FULL_VERSION} -X main.builtAt=${BUILD_AT}"

build_docker:
	docker rm -f ncc-clicker || true
	docker run -d --name ncc-clicker -v $$(pwd):/go/src/ncc-clicker -w /go/src/ncc-clicker docker.cc.naumen.ru:443/golang_java:latest tail -f /dev/fd/0
	docker exec -ti ncc-clicker go-wrapper download
	docker exec -ti ncc-clicker go test -v
	docker exec -ti ncc-clicker ${BUILD_COMMAND}

build_local:
	go-wrapper download
	go test -v
	$(BUILD_COMMAND)

deploy:
	curl --insecure ${CURL_FORM_PARAMS} -F file=@ncc-clicker http://repo.cc.naumen.ru/upload/
