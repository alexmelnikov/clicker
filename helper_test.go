package main

import (
	"net/url"
	"strings"
	"testing"
)

func TestProcessResponse(t *testing.T) {
	input := `
		<a href="/page1">hello</a>
		<a name="/page2" rel="/page2">bahaha!</a>
		<a href="/page3">goodbye</a>
		<div onclick="open_in_new_window(window.event, '');"/>
		<div onclick='open_in_new_window(window.event, "");'/>
		<a onclick="open_in_new_window(window.event, '/fx/page4');"/>
		<div onclick='open_in_new_window(window.event, "/fx/page5");'/>
		<img onclick="open_in_new_window(window.event, '/fx/page6');"/>
		<span onclick="open_in_new_window(window.event, '/fx/page7');"/>
		`
	expectArray := []string{
		"/page1", "/page3", "/fx/page4", "/fx/page5", "/fx/page6",
	}

	chSeed := make(chan *linkInfo, len(expectArray))
	processResponse(200, strings.NewReader(input), &linkInfo{}, chSeed, nil)

	for _, expect := range expectArray {
		select {
		case got := <-chSeed:
			if expect != got.link {
				t.Errorf("Parse error\nExpect: %s\nGot:    %s\n", expect, got.link)
			}
		default:
			t.Errorf("Parse error\nExpect: %s\n", expect)
		}
	}
}

func TestProcessResponseWithError(t *testing.T) {
	expectArr := map[string]string{
		// css: .message-error
		`<html><body>
			<div id="messageContainer_editProperty" class="message-container">
				<div class="message-error">
				Параметр /Root/InterfaceConfig/ActiveXCallInfoConfig/CallInfoProvider отсутствует в справочнике &quot;Параметры профилей&quot;. Пожалуйста, импортируйте новые параметры в справочник.
				</div>
			</div>
		</body></html>
		`: "Параметр /Root/InterfaceConfig/ActiveXCallInfoConfig/CallInfoProvider " +
			"отсутствует в справочнике \"Параметры профилей\". Пожалуйста, импортируйте " +
			"новые параметры в справочник.",
		// css: .error
		`<html><body>
			<span class="error">
				File for playback has not been found
			</span>
		</body></html>
		`: "File for playback has not been found",

		// Logon form
		`<html><body>
			<form name="LogonForm">
				some fields inside
			</form>
		</body></html>
		`: "LogonForm",

		// Stack trace on page
		`<html><body>
			<div name="stackTrace">
				some fields inside
			</div>
		</body></html>
		`: "StackTrace",
	}

	for input, expect := range expectArr {
		chBroken := make(chan *linkInfo, 1)
		processResponse(200, strings.NewReader(input), &linkInfo{}, nil, chBroken)
		select {
		case got := <-chBroken:
			if expect != got.err {
				t.Errorf("Parse error\nExpect: %s\nGot:    %v\n", expect, got.err)
			}
		default:
			t.Errorf("Parse error\nExpect error: %s\n", expect)
		}
		close(chBroken)
	}
}

func TestFindError400Message(t *testing.T) {
	expectArr := map[string]string{
		// css: .message-error
		`<html><body>
			<div id="messageContainer_editProperty" class="message-container">
				<div class="message-error">
				Параметр /Root/InterfaceConfig/ActiveXCallInfoConfig/CallInfoProvider отсутствует в справочнике &quot;Параметры профилей&quot;. Пожалуйста, импортируйте новые параметры в справочник.
				</div>
			</div>
		</body></html>
		`: "Параметр /Root/InterfaceConfig/ActiveXCallInfoConfig/CallInfoProvider " +
			"отсутствует в справочнике \"Параметры профилей\". Пожалуйста, импортируйте " +
			"новые параметры в справочник.",
		// css: .error
		`<html><body>
			<span class="error">
				File for playback has not been found
			</span>
		</body></html>
		`: "File for playback has not been found",

		// Logon form
		`<html><body>
			<form name="LogonForm">
				some fields inside
			</form>
		</body></html>
		`: "",

		// Stack trace on page
		`<html><body>
			<div name="stackTrace">
				some fields inside
			</div>
		</body></html>
		`: "",
	}

	for input, expect := range expectArr {
		got := findError400Message(strings.NewReader(input))
		if expect != got {
			t.Errorf("\nInput:  %s\nExpect: %s\nGot:    %s\n", input, expect, got)
		}
	}
}

func TestAlllowedLinks(t *testing.T) {
	rootURL, _ := url.Parse("http://127.0.0.1:8080")
	fromLink := "http://127.0.0.1:8080/fx/published?uuid=corebo00000000000lvuvmgmb5fvi1q4"

	values := map[string]string{
		"":  "",
		"#": "",
		"https://127.0.0.1:8080/fx/wrongScheme":      "",
		"http://192.168.0.1:8080/fx/externalAddress": "",
		"http://127.0.0.1:8443/fx/wrongPort":         "",
		"sip://123":                                  "",
		"h323://123":                                 "",
		"\\\\127.0.0.1\\SambaURL\\Файлы":             "",
		"mailto://test@naumen.ru":                    "",
		"http://google.com/fx/externalHost":          "",
		"/fx/fs/record?session_id=sess_123":          "",
		"/fx/monitoring":                             "",
		"/monitoring":                                "",
		"/fx/logout":                                 "",
		"/logout":                                    "",
		"/fx/published?activeComponent=stdViewpart0.frmRebuild":           "",
		"/fx/published?activeComponent=IASPartnersRemoved":                "http://127.0.0.1:8080/fx/published?activeComponent=IASPartnersRemoved",
		"http://127.0.0.1:8080/fx/sort?uuid=corebo1&activeObject=corebo2": "http://127.0.0.1:8080/fx/sort?activeObject=corebo2&uuid=corebo1",
		"http://127.0.0.1:8080/fx/static/":                                "http://127.0.0.1:8080/fx/static/",
		"/fx/npo/ru.naumen.npo.published_jsp?uuid=123&objs_pn=2":          "http://127.0.0.1:8080/fx/npo/ru.naumen.npo.published_jsp?objs_pn=2&uuid=123",
		"/fx/published?uuid=corebo1&activeObject=corebo2":                 "http://127.0.0.1:8080/fx/published?activeObject=corebo2&uuid=corebo1",
		"/fx/published":                                                   "http://127.0.0.1:8080/fx/published",
		"/fx/published?uuid=123&objs_pn=0":                                "http://127.0.0.1:8080/fx/published?uuid=123",
		"/fx/published?uuid=123&objs_pn=2":                                "http://127.0.0.1:8080/fx/published?objs_pn=2&uuid=123",
		"?uuid=corebo00000000000lvuvmgmb5fvi1q&tablelist_pn=1":            "http://127.0.0.1:8080/fx/published?tablelist_pn=1&uuid=corebo00000000000lvuvmgmb5fvi1q",
	}

	for input, expect := range values {
		got := parseOpmsLink(input, fromLink, rootURL)

		if expect != got {
			t.Errorf("\nInput:  %s\nExpect: %s\nGot:    %s\n", input, expect, got)
		}
	}
}

func TestSearchJsLink(t *testing.T) {
	scripts := map[string]string{
		"open_in_new_window(window.event, '/fx/page1');":         "/fx/page1",
		`open_in_new_window(someFunction(), "/fx/page2", null);`: "/fx/page2",
		"open_in_new_window(window.event, '/somepage');":         "/somepage",
		"open_in_new_window(window.event, '');":                  "",
		"other_function('string');":                              "",
		"broken script(":                                         "",
		"open_in_new_window(window.event, '/fx/delEte');":        "",
		"open_in_new_window(window.event, '/fx/rEmove');":        "",
		"if (confirmMsg(window.event, 'some_message') \n" +
			" { open_in_new_window(window.event, '/fx/page3'); }": "/fx/page3",
	}

	for input, expect := range scripts {
		got := searchJsLink(input)
		if expect != got {
			t.Errorf("\nInput:  %s\nExpect: %s\nGot:    %s\n", input, expect, got)
		}
	}
}

func TestRootPath(t *testing.T) {
	roots := map[string]string{
		"http://127.0.0.1":                             "http://127.0.0.1",
		"HttP://127.0.0.1":                             "http://127.0.0.1",
		"http://127.0.0.1/":                            "http://127.0.0.1",
		"http://stand217.naumen.ru/":                   "http://stand217.naumen.ru",
		"http://stand217.naumen.ru:8080":               "http://stand217.naumen.ru:8080",
		"http://127.0.0.1:8080/fx/auth":                "http://127.0.0.1:8080",
		"http://127.0.0.1:8080/fx/published#":          "http://127.0.0.1:8080",
		"http://127.0.0.1:8080/fx/published?uuid=123#": "http://127.0.0.1:8080",
	}

	for input, expect := range roots {
		got := getRootPath(input).String()
		if expect != got {
			t.Errorf("\nInput:  %s\nExpect: %s\nGot:    %s\n", input, expect, got)
		}
	}
}

func TestIncorrectRootPath(t *testing.T) {
	roots := []string{
		"stand217",
		";dfksg;sflm",
		"stand217.naumen.ru",
		"stand217.naumen.ru",
		"smb://stand217.naumen.ru/someURL",
		"http://",
	}

	for _, input := range roots {
		func() {
			defer func() {
				if r := recover(); r == nil {
					t.Errorf("Should panic on link: " + input)
				}
			}()

			getRootPath(input)
		}()
	}
}
