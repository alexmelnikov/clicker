package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"sort"
	"strings"
	"time"
)

type linkInfo struct {
	link     string
	from     string
	start    *time.Time
	duration int
	err      interface{}
	count    int
}

type clicker struct {
	logger *log.Logger
	opts   options
}

func (c *clicker) run() {
	c.logger = createLogger(c.opts.Output)
	var timeLogger *log.Logger
	if c.opts.Timings {
		name := strings.Replace(c.opts.Output, ".log", "_timings.csv", 1)
		timeLogger = createLogger(name)
	} else {
		timeLogger = nil
	}

	client := &http.Client{
		Timeout: 120 * time.Second,
		Transport: &http.Transport{
			// игнорируем некорректные сертификаты
			TLSClientConfig:    &tls.Config{InsecureSkipVerify: true},
			MaxIdleConns:       10,
			IdleConnTimeout:    30 * time.Second,
			DisableCompression: true,
		},
	}

	chSeed := make(chan *linkInfo, 1)
	chFinished := make(chan *linkInfo)
	chBroken := make(chan *linkInfo)

	visitedLinks := make(map[string]bool)
	brokenLinks := make(map[string]*linkInfo)

	// start with page
	rootURL := getRootPath(c.opts.Root)
	chSeed <- &linkInfo{link: rootURL.String() + "/fx/auth"}

	queue := make([]*linkInfo, 0, 100)

	chInterrupt := make(chan os.Signal, 1)
	signal.Notify(chInterrupt, os.Interrupt)
	var activeWorkers int

EVENTS:
	for {
		select {
		case info := <-chSeed:
			link := parseOpmsLink(info.link, info.from, rootURL)

			if link == "" {
				continue
			}

			info.link = link

			if !visitedLinks[link] {
				queue = append(queue, info)
				visitedLinks[link] = true
			}
		case bl := <-chBroken:
			baseURL, _ := c.extractBaseOpmsLink(bl.link)
			key := fmt.Sprintf("%v,%s", bl.err, baseURL)
			if props, exist := brokenLinks[key]; !exist {
				bl.count = 1
				brokenLinks[key] = bl
			} else {
				count := props.count + 1

				// Выбираем "минимальную" ссылку для постоянства результатов теста
				if bl.link < props.link {
					bl.count = count
					brokenLinks[key] = bl
				} else {
					props.count = count
				}
			}
		case info := <-chFinished:
			activeWorkers--
			if timeLogger != nil {
				var status string
				if info.err == nil {
					status = "OK"
				} else {
					status = "ERROR"
				}
				timeLogger.Printf("%s,%s,%s,%d,%s\n", info.from, info.link, info.start.Format(time.StampMicro), info.duration, status)
			}

			// выходим из цикла, если закончилась очередь и обработчики
			if len(queue) == 0 && activeWorkers == 0 {
				break EVENTS
			}
		case <-chInterrupt:
			break EVENTS
		}

		// 1. Ограничиваем количество горутин по количеству ядер, чтобы не DDOS-ить OPMS
		// 2. Если какие то горутины отработали, то запускаем ещё пока есть ядра и задания в очереди
		for activeWorkers < c.opts.Jobs && len(queue) > 0 {
			activeWorkers++
			go func(info *linkInfo) {
				c.extractHREFs(info, client, chSeed, chBroken)
				chFinished <- info
			}(queue[0])

			queue[0] = nil
			queue = queue[1:]
		}
	}

	c.printBroken(brokenLinks)
	c.logger.Printf("Total links visited: %d\n", len(visitedLinks))

	c.printStatistics(visitedLinks)
}

func (c *clicker) extractHREFs(info *linkInfo, client *http.Client, chSeed chan *linkInfo, chBroken chan *linkInfo) {

	start := time.Now()
	info.start = &start

	req, err := http.NewRequest("GET", info.link, nil)
	if err != nil {
		info.err = fmt.Sprintf("Error on create request: %v", err)
		chBroken <- info
		return
	}
	req.SetBasicAuth(c.opts.User, c.opts.Password)
	resp, err := client.Do(req)
	if err != nil {
		info.err = fmt.Sprintf("Error on GET request: %v", err)
		chBroken <- info
		return
	}
	defer resp.Body.Close()

	info.duration = int(time.Now().Sub(start).Nanoseconds() / 1000000)

	contentType := resp.Header.Get("content-type")
	if !strings.Contains(contentType, "text/html") {
		return
	}

	processResponse(resp.StatusCode, resp.Body, info, chSeed, chBroken)
}

func (c *clicker) extractBaseOpmsLink(href string) (string, *url.Values) {
	q, err := url.Parse(href)
	if err != nil {
		c.logger.Println("Can't parse href: ", err)
	}
	values := q.Query()

	prefixValues := &url.Values{}
	for _, key := range []string{"uuid", "view_uuid", "session_id", "targetUUID"} {
		uuid := values.Get(key)
		if len(uuid) < 6 {
			continue
		}

		prefix := []rune(uuid)
		prefixValues.Set(key, string(prefix[:6]))
	}

	q.RawQuery = prefixValues.Encode()
	return q.String(), prefixValues
}

func (c *clicker) printBroken(brokenLinks map[string](*linkInfo)) {
	keys := make([]string, 0, len(brokenLinks))
	for key := range brokenLinks {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	for _, key := range keys {
		val, _ := brokenLinks[key]

		c.logger.Printf("ERROR: %v\n  link: %s\n  from: %s\n  count: %d\n", val.err, val.link, val.from, val.count)
	}
}

/**
 * Печать распределения ссылок по uuid-ам.
 * Количество появлений различных префиксов uuid-ов у посещаемых
 * ссылок позволяет понять, почему кликер ведёт себя неожиданным
 * образом.
 */
func (c *clicker) printStatistics(visitedLinks map[string]bool) {
	if c.opts.Statistics {
		occurrences := make(map[string]int, 50)
		for link := range visitedLinks {
			_, prefixes := c.extractBaseOpmsLink(link)
			uuidPrefix := prefixes.Get("uuid")
			if _, exist := occurrences[uuidPrefix]; exist {
				occurrences[uuidPrefix] = occurrences[uuidPrefix] + 1
			} else {
				occurrences[uuidPrefix] = 1
			}
		}

		c.logger.Println("== UUID distribution ==")
		//TODO sorted by count
		for prefix, cnt := range occurrences {
			c.logger.Printf("%s: %d\n", prefix, cnt)
		}
	}
}

func createLogger(filename string) *log.Logger {
	f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v\n", err)
	}

	return log.New(f, "", 0)
}
