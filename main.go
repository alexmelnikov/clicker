package main

import (
	"fmt"
	"net"
	"os"
	"runtime"

	arg "github.com/alexflint/go-arg"
)

type options struct {
	Root       string `arg:"-r" help:"Root URL"`
	User       string `arg:"-u" help:"User"`
	Password   string `arg:"-p" help:"Password for given user"`
	Output     string `arg:"-o" help:"File used for logging"`
	Jobs       int    `arg:"-j" help:"Number of parallel workers"`
	Iterations int    `arg:"-i" help:"How many times clicker should be run"`
	Statistics bool   `arg:"-s" help:"Print statistical distribution of pages"`
	Timings    bool   `arg:"-t" help:"Save page download timings"`
}

func (*options) Description() string {
	return "Кликер обходит веб-приложение и выводит список битых страниц"
}

var (
	commit  = "devel"
	builtAt = ""
)

func main() {
	fmt.Printf("Version '%s'\n", commit)
	if builtAt != "" {
		fmt.Printf("Build at '%s'\n", builtAt)
	}

	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}

	addr, err := net.LookupIP(hostname)
	if err != nil {
		panic(err)
	}

	opts := options{
		Root:       "http://" + addr[0].String() + ":8080",
		User:       "system",
		Password:   "manager",
		Output:     "clicker.log",
		Jobs:       runtime.NumCPU(),
		Iterations: 1,
		Statistics: false,
		Timings:    true,
	}

	arg.MustParse(&opts)

	for i := 0; i < opts.Iterations; i++ {
		clk := &clicker{opts: opts}

		clk.run()
	}
}
