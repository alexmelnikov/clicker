package main

import (
	"fmt"
	"io"
	"net/url"
	"regexp"
	"sort"
	"strings"

	"golang.org/x/net/html"
)

func getAttr(t *html.Token, attrName string) string {
	for _, a := range t.Attr {
		if a.Key == attrName {
			return a.Val
		}
	}

	return ""
}

func processResponse(statusCode int, body io.Reader, currentInfo *linkInfo, chSeed chan *linkInfo, chBroken chan *linkInfo) {
	if statusCode == 400 {
		msg := findError400Message(body)

		if msg != "" {
			currentInfo.err = fmt.Sprintf("%d %s", statusCode, msg)
		} else {
			currentInfo.err = statusCode
		}
		chBroken <- currentInfo
		return
	}

	if statusCode > 400 {
		currentInfo.err = statusCode
		chBroken <- currentInfo
		return
	}

	tokenizer := html.NewTokenizer(body)

	for {
		tt := tokenizer.Next()

		switch tt {
		case html.ErrorToken:
			// End of the document, we're done
			return
		case html.StartTagToken, html.SelfClosingTagToken:
			t := tokenizer.Token()

			if t.Data == "a" {
				href := getAttr(&t, "href")
				if href != "" {
					chSeed <- &linkInfo{link: href, from: currentInfo.link}
				}
			}

			if t.Data == "a" || t.Data == "div" || t.Data == "img" {
				onclick := getAttr(&t, "onclick")
				if onclick != "" {
					href := searchJsLink(onclick)
					if href != "" {
						chSeed <- &linkInfo{link: href, from: currentInfo.link}
					}
				}
			}

			if t.Data == "form" && getAttr(&t, "name") == "LogonForm" {
				chBroken <- &linkInfo{link: currentInfo.link, from: currentInfo.from, err: "LogonForm"}
			}

			if t.Data == "div" && getAttr(&t, "name") == "stackTrace" {
				chBroken <- &linkInfo{link: currentInfo.link, from: currentInfo.from, err: "StackTrace"}
			}

			class := getAttr(&t, "class")
			if class == "message-error" || class == "error" {
				message := getTokenText(tokenizer)
				chBroken <- &linkInfo{link: currentInfo.link, from: currentInfo.from, err: message}
			}
		}
	}
}

/**
 * Ищем ошибку на форме, если это 400
 */
func findError400Message(body io.Reader) string {
	tokenizer := html.NewTokenizer(body)

	for {
		tt := tokenizer.Next()

		switch tt {
		case html.ErrorToken:
			// End of the document, we're done
			return ""
		case html.StartTagToken, html.SelfClosingTagToken:
			t := tokenizer.Token()

			class := getAttr(&t, "class")
			if class == "message-error" || class == "error" {
				return getTokenText(tokenizer)
			}
		}
	}

	return ""
}

func getTokenText(tokenizer *html.Tokenizer) string {
	level := 1
	message := ""
	for level > 0 {
		ett := tokenizer.Next()
		switch ett {
		case html.TextToken:
			et := tokenizer.Token()
			message += strings.TrimSpace(et.Data)
		case html.StartTagToken:
			level++
		case html.EndTagToken:
			level--
		}
	}

	return message
}

func parseOpmsLink(link string, from string, rootURL *url.URL) string {
	if link == "" {
		return ""
	}

	q, err := url.Parse(link)
	if err != nil {
		// ignore unparsable link
		return ""
	}

	// skip totally empty links
	if q.Scheme == "" && q.Host == "" && q.Path == "" && q.RawQuery == "" {
		return ""
	}

	// convert to absolute link
	if q.Scheme == "" || q.Host == "" || q.Path == "" {
		fromURL, _ := url.Parse(from)

		if q.Scheme == "" {
			q.Scheme = fromURL.Scheme
		}
		if q.Host == "" {
			q.Host = fromURL.Host
		}
		if q.Path == "" {
			q.Path = fromURL.Path
		}
	}

	path := q.Path
	if q.Scheme != rootURL.Scheme || q.Host != rootURL.Host || // ignore external links
		!strings.HasPrefix(path, "/") || // only OPMS links
		strings.HasPrefix(path, "/fx/logout") || strings.HasPrefix(path, "/logout") || // do not logout
		strings.HasPrefix(path, "/fx/monitoring") || strings.HasPrefix(path, "/monitoring") || // ignore monitoring
		strings.HasPrefix(path, "/fx/fs/") || // ignore requests to File Service
		strings.Contains(path, "RecordsDownloadServlet") ||
		strings.Contains(path, "callrecord") ||
		q.Query().Get("activeComponent") == "stdViewpart0.frmRebuild" {
		return ""
	}

	sortParameters(q)

	return q.String()
}

/**
 * Сортируем параметры, чтобы ссылки всегда выглядели одинаково
 * Так же убираем неважные параметры
 */
func sortParameters(q *url.URL) {
	pairs := q.Query()
	if len(pairs) == 0 {
		return
	}

	keys := make([]string, 0, len(pairs))
	for key, value := range pairs {
		if !(strings.HasSuffix(key, "_pn") && value[0] == "0") &&
			!strings.HasSuffix(key, "sort_dir") &&
			!strings.HasSuffix(key, "sort_column") &&
			!strings.HasSuffix(key, "active_tab") {

			keys = append(keys, key)
		}
	}

	sort.Strings(keys)

	params := make([]string, 0, len(keys))
	for _, key := range keys {
		params = append(params, url.QueryEscape(key)+"="+url.QueryEscape(pairs[key][0]))
	}

	q.RawQuery = strings.Join(params, "&")
}

// текст, начинающийся со слеша ('/'), в двойных или одинарных кавычках внутри open_in_new_window(...)
var reOpenInNewWindow = regexp.MustCompile(`open_in_new_window\(.+['\"](/.+?)['\"].*\)`)

// Для простоты (дабы избежать тяжёлого разбора JavaScript)
// просто ищем по regexp
func searchJsLink(onclick string) string {
	strs := reOpenInNewWindow.FindAllStringSubmatch(onclick, -1)
	if strs == nil || len(strs) < 1 {
		return ""
	}

	link := strs[0][1]
	linkLower := strings.ToLower(link)

	// skip danger link
	if strings.Contains(linkLower, "delete") || strings.Contains(linkLower, "remove") {
		return ""
	}

	return link
}

func getRootPath(rootPath string) *url.URL {
	rootURL, err := url.Parse(rootPath)
	if err != nil {
		panic(err)
	}
	if rootURL.Scheme == "" || rootURL.Host == "" {
		panic("Incorrect root url")
	}
	if rootURL.Scheme != "http" && rootURL.Scheme != "https" {
		panic("Scheme not supported: " + rootURL.Scheme)
	}

	rootURL.Path = ""
	rootURL.RawPath = ""
	rootURL.RawQuery = ""
	return rootURL
}
